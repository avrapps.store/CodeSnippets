### Usage
```
            AboutUtils aboutUtils = new AboutUtils(mContext);
            switch (preference.getKey()) {
                case "report_bug":
                    aboutUtils.shareFeedback();
                    break;
                case "rate_us":
                    aboutUtils.reateus();
                    break;
                case "share_app":
                    aboutUtils.shareApp();
                    break;
                case "more_apps":
                    aboutUtils.moreAppsFromPlayStore("pub:AVR-Apps");
                    break;
                case "download_icon_pack":
                    aboutUtils.moreAppsFromPlayStore("Icon Packs");
                    break;
                default:
                    Toast.makeText(mContext, "No Preference found for the key : " +
                            preference.getKey(), Toast.LENGTH_LONG).show();
            }
            ```
### Strings
```
    <string name="app_feedbac">Top Launcher Feedback</string>
    <string name="error_email_client">No Email App Found</string>
```
### AboutUtils.java
```
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import com.avrapps.pixellauncher.R;

public class AboutUtils {

    Context c;
    public AboutUtils(Context c){
        this.c=c;
    }

    public void shareApp(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this interesting app at: https://play.google.com/store/apps/details?id=" + c.getPackageName());
        sendIntent.setType("text/plain");
        c.startActivity(sendIntent);
    }

    public void shareFeedback(){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"avrapps.store@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, c.getString(R.string.app_feedbac));
        i.putExtra(Intent.EXTRA_TEXT, c.getString(R.string.feedback));
        try {
            c.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(c, R.string.error_email_client, Toast.LENGTH_SHORT).show();
        }
    }

    public void reateus(){
        Uri uri = Uri.parse("market://details?id=" + c.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }
        try {
            c.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            c.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + c.getPackageName())));
        }
    }

    public void moreAppsFromPlayStore(String keyword){
        Uri uri = Uri.parse("market://search?q="+keyword);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }
        try {
            c.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            c.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/search?q=pub:"+keyword)));
        }
    }
}

```